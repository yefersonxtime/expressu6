var express = require("express");
var bodyParse = require("body-parser");

var app = express (); 
app.use (express.json()); //agrega unas dependencias adicionales
app.use(express.urlencoded ( {extended: true})); //urlencoded parse las solucitudes desde url
app.use( (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, XRequested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods','GET, POST, PUT, DELETE, OPTIONS');
    res.header('Allow', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
}); //esta funcion arrow va usarse solo aqui req=recibe, res=devuelve, next=para despues

module.exports = app;
